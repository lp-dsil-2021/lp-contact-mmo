import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class ContactServiceMockTest {

    @Mock
    private IContactDAO contactDAO;

    @InjectMocks
    private ContactService contactService = new ContactService();

    @Test
    public void shouldFailOnDuplicateEntry(){
        Mockito.when(contactDAO.isContactExist("Thierry")).thenReturn(true);
        Assertions.assertThrows(ContactException.class, () -> contactService.creerContact("Thierry"));
    }

    @Test
    public void shouldPass() throws ContactException {
        Mockito.when(contactDAO.isContactExist("Thierry")).thenReturn(false);
        contactService.creerContact("Thierry");
    }

    @Test
    public void shouldFailOnRemoveEmptyEntry() {
    }


}
