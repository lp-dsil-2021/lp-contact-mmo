import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ContactServiceTest {

    ContactService service = new ContactService();

    @Test
    public void shouldFailIfNull() {
        assertThrows(ContactException.class, () -> service.creerContact(null));

    }

    @Test
    public void shouldFailIfBlank() {
        assertThrows(ContactException.class, () -> service.creerContact("   "));
    }

    @Test
    public void shouldFailIfEmpty() {
        assertThrows(ContactException.class, () -> service.creerContact(""));
    }

    @Test
    public void shouldFailIfUnderThree() {
        assertThrows(ContactException.class, () -> service.creerContact("ab"));
    }

    @Test
    public void shouldFailIfOverForty() {
        assertThrows(ContactException.class, () -> service.creerContact("azertyuiopmlkjhgfdsqwxcvbnazertyuiopmlkjhgfdsqwxcvbn"));
    }

    @Test
    public void shouldPass() throws ContactException {
        service.creerContact("leo");
        service.creerContact("thierry");
    }

    @Test
    public void shouldFailOnDuplicate() throws ContactException {
        service.creerContact("thierry");
        assertThrows(ContactException.class, () -> service.creerContact("thierry"));
    }

}
