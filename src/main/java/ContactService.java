public class ContactService{

    public IContactDAO contactDAO = new ContactDAO();


    public void creerContact(String nom) throws ContactException {
        if(nom == null|| nom.trim().length() < 3 || nom.trim().length() > 40) {
            throw new ContactException();
        }
        if(contactDAO.isContactExist(nom)) {
            throw new ContactException();
        }

        Contact contact = new Contact(nom);
        contactDAO.add(contact);

    }

    public void suppContact(String nom) throws ContactException {
        if(nom == null|| nom.trim().length() < 3 || nom.trim().length() > 40) {
            throw new ContactException();
        }

        if(!contactDAO.isContactExist(nom)) {
            throw new ContactException();
        }

        contactDAO.remove(nom);
    }
}
