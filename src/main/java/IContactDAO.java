import java.util.ArrayList;
import java.util.List;

public interface IContactDAO {
    void add(Contact c);
    void remove(String nom);
    boolean isContactExist(String name);
}
