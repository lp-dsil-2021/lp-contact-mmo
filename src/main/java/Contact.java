public class Contact {

    String name;

    Contact(String name) {
        this.name = name;
    }

    String getName() {
        return this.name;
    }
}
