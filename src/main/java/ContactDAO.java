import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ContactDAO implements IContactDAO {
    public List<Contact> contacts = new ArrayList<>();


    public void add(Contact c) {
        contacts.add(c);
    }

    public void remove(String nom) {
        Optional<Contact> c = contacts.stream().filter(x -> nom.equals(x.getName()))
                .findAny();
        contacts.remove(c);
    }


    public boolean isContactExist(String name) {
        return contacts.stream().anyMatch(x->name.equalsIgnoreCase(x.getName()));
    }


}
